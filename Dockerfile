FROM ruby:latest

ADD . /var/services/itunes-graphql
WORKDIR /var/services/itunes-graphql

RUN ./scripts/setup
CMD ./scripts/init
